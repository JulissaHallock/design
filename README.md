Learn the Basic Scale Drawing
======

I have seen the basic floor plan and professionally made from advance technology, which 2D floor plan has been converted into a quaint 3D Floor plan. Have you wondered how artists, architects, and engineers made the image in no time? There are several steps of how to make one, and learning the basic is a must.

The photos below are made from advance technology when I was in Singapore. The [Interior design](http://www.0932design.sg/) is made from a casual but stylish idea from people I met years ago. When you wanted to start your own design, start with the objects, the colors and measurement of each subject.

2D Floor Plan
======
![Screenshot of 2D](http://sub-zeroanimation.com/wp-content/gallery/private-residential-house-2d-floor-plans/3d-studio-ho-chi-minh-private-residential-house-2d-floor-plans-7.jpg)

3D Floor Plan
======
![Screenshot of 2D](http://home4lifenow.com/wp-content/uploads/2012/07/Floor-Plans-For-New-Homes-500x382.jpg)

![Screenshot of 2D](http://wazocommunications.com/wp-content/uploads/2013/02/5th-without-water-mark.jpg)